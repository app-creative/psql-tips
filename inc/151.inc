    <div class="tip">
      The <code>\password username</code> will change the password of the specified user.
      This command prompts for the new password, encrypts it, and sends it to
      the server as an ALTER ROLE command. This makes sure that the new password
      does not appear in cleartext in the command history, the server log, or
      elsewhere. That's the secure way to change passwords in Postgres.
      <pre><code class="hljs bash">laetitia=# \password test
      Enter new password for user "test":
      Enter it again:
      laetitia=#</code></pre>This feature is available
since Postgres 8.2.
	</div>
